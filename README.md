# wallpaper repository of quantenzitrone

## examples

![example1](all-wallpapers/astronaut-holding-space-jellyfish.png)
![example2](all-wallpapers/carcharoth.png)
![example3](all-wallpapers/thefatrat-mayday.png)
![example4](all-wallpapers/enjoing-the-sunset-on-a-cliff.png)
![example5](all-wallpapers/aenami-idea-of-you.png)
![example6](all-wallpapers/foggy-mountain-woods.png)
![example7](all-wallpapers/mountains.png)
![example8](all-wallpapers/red-waterfall-solar-eclipse.png)
![example9](all-wallpapers/scratcherpen-the-witcher-misty-dark-forest.png)

## License / Copyright

I have no rights to these pictures, as they are pictures I gathered over time online from various sources.

If you are the copyright holder of one or more of these pictures and want them taken down, please contact me via either an issue on this repository, or via Matrix on @quantenzitrone:matrix.org. You may also find me on various other platforms on the internet.
