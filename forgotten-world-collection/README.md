!! downscaled images for faster loadtime, click to get to the full resolution image !!

[![forgotten-world-angkor-wat](../icons/forgotten-world-angkor-wat.webp)](../all-wallpapers/forgotten-world-angkor-wat.png)
[![forgotten-world-capitol](../icons/forgotten-world-capitol.webp)](../all-wallpapers/forgotten-world-capitol.png)
[![forgotten-world-colosseum](../icons/forgotten-world-colosseum.webp)](../all-wallpapers/forgotten-world-colosseum.png)
[![forgotten-world-gizeh](../icons/forgotten-world-gizeh.webp)](../all-wallpapers/forgotten-world-gizeh.png)
[![forgotten-world-hungarian-parliament](../icons/forgotten-world-hungarian-parliament.webp)](../all-wallpapers/forgotten-world-hungarian-parliament.png)
[![forgotten-world-saint-basils](../icons/forgotten-world-saint-basils.webp)](../all-wallpapers/forgotten-world-saint-basils.png)
[![forgotten-world-schwanstein](../icons/forgotten-world-schwanstein.webp)](../all-wallpapers/forgotten-world-schwanstein.png)
[![forgotten-world-taj-mahal](../icons/forgotten-world-taj-mahal.webp)](../all-wallpapers/forgotten-world-taj-mahal.png)
