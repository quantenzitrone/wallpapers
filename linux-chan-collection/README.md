!! downscaled images for faster loadtime, click to get to the full resolution image !!

[![arch-linux-chan-despair](../icons/arch-linux-chan-despair.webp)](../all-wallpapers/arch-linux-chan-despair.png)
[![arch-linux-chan](../icons/arch-linux-chan.webp)](../all-wallpapers/arch-linux-chan.png)
[![arch-linux-chan2](../icons/arch-linux-chan2.webp)](../all-wallpapers/arch-linux-chan2.png)
[![arch-linux-chan3](../icons/arch-linux-chan3.webp)](../all-wallpapers/arch-linux-chan3.png)
[![athena-chan](../icons/athena-chan.webp)](../all-wallpapers/athena-chan.png)
[![athena-chan2](../icons/athena-chan2.webp)](../all-wallpapers/athena-chan2.png)
[![cybersec-chan](../icons/cybersec-chan.webp)](../all-wallpapers/cybersec-chan.png)
[![hyprland-chan0](../icons/hyprland-chan0.webp)](../all-wallpapers/hyprland-chan0.png)
[![hyprland-chan1](../icons/hyprland-chan1.webp)](../all-wallpapers/hyprland-chan1.png)
[![hyprland-chan2](../icons/hyprland-chan2.webp)](../all-wallpapers/hyprland-chan2.png)
[![lfs-chan](../icons/lfs-chan.webp)](../all-wallpapers/lfs-chan.png)
[![manjaro-chan](../icons/manjaro-chan.webp)](../all-wallpapers/manjaro-chan.png)
[![void-linux-chan](../icons/void-linux-chan.webp)](../all-wallpapers/void-linux-chan.png)
