!! downscaled images for faster loadtime, click to get to the full resolution image !!

[![scratcherpen-the-witcher-misty-dark-forest](../icons/scratcherpen-the-witcher-misty-dark-forest.webp)](../all-wallpapers/scratcherpen-the-witcher-misty-dark-forest.png)
[![the-witcher-hanged-mans-tree](../icons/the-witcher-hanged-mans-tree.webp)](../all-wallpapers/the-witcher-hanged-mans-tree.png)
[![the-witcher-kaer-morhen](../icons/the-witcher-kaer-morhen.webp)](../all-wallpapers/the-witcher-kaer-morhen.png)
[![the-witcher-novigrad-harbour](../icons/the-witcher-novigrad-harbour.webp)](../all-wallpapers/the-witcher-novigrad-harbour.png)
[![the-witcher-swampy-river](../icons/the-witcher-swampy-river.webp)](../all-wallpapers/the-witcher-swampy-river.png)
