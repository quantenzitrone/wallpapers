#!/usr/bin/env fish

for i in ./all-wallpapers/*.png
    set name (string split '.' (string split '/' $i)[3])[1]
    if ! test -e ./icons/$name.webp
        magick $i -resize 500x -quality 80 -define webp:lossless=false ./icons/$name.webp
    end
end
