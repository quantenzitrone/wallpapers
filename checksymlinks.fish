#!/usr/bin/env fish

for i in *-collection/*.png
	if test ! -L $i
		echo $i is not a symbolic link
	else if test ! -e $i
		echo "symlink $i->$(readlink $i) is broken"
	end
end
