#!/usr/bin/env fish

for i in all-wallpapers/*.png
	set width (identify -ping -format "%w" $i)
	set height (identify -ping -format "%h" $i)
	if test $width -lt 1920 -o $height -lt 1080
		echo -s $i " " $width x $height
	end
end
