#!/usr/bin/env fish

./generateicons.fish

for i in ./*-collection ./all-wallpapers
	echo $i
	printf "!! downscaled images for faster loadtime, click to get to the full resolution image !!\n\n" > $i/README.md
	for j in $i/*.png
		set name (string split '.' (string split '/' $j)[3])[1]
		echo "[![$name](../icons/$name.webp)](../all-wallpapers/$name.png)" >> $i/README.md
	end
end
